import numpy as np 


class Momentum:

	"""
	General Momentum Mesh Class to create equidistant mesh 
	and irreducible path.
	"""
	
	def __init__(self, G1, G2): 
		
		"""
		Parameters
		----------
		Arguments: G1, G2 - Reciprocal lattice vectors
		"""

		#Reciprocal lattice vectors
		self.G1, self.G2 = G1, G2


	def Bravais_mesh_simple(self, Nx, Ny=None): 

		"""
		Create simple Bravais mesh with Nx point along self.G1
		and Ny points along self.G2
		"""
		if Ny is None: Ny = Nx
		mesh = np.zeros((Nx,Ny,3), dtype=float)
		
		for i in range(Nx): 
			for j in range(Ny):
				mesh[i,j] = i/Nx*self.G1 + j/Ny*self.G2

		mesh = mesh.reshape(Nx*Ny,3)

		return mesh 


	def k_path(self, k_points, irr_path): 

		"""
		Set up irreducible path along points in irr_path with given number
		of k_points. Algorithm sets number of kpoints accoridng to actual 
		distance of high-symmetry point in momentum space.

		Arguments: 		k_points - int, Number of kpoints along irr. path
						irr_path - List of 3d Arrays with coordinates of high symmetry points


		Return:         k_path - List of 3d Arrays with momentum points on irr. path
						marker - List of Int indicating the position of high symmetry points
		"""

		#Compute distances between high symmetry points
		high_sym = irr_path
		distances = np.zeros(len(high_sym)-1, dtype = float)
		for i in range(len(distances)):
			distances[i] = np.linalg.norm(high_sym[i]-high_sym[i+1])
		d_tot = np.sum(distances)

		#Distribute points uniformly over the intervals
		number_points=np.zeros_like(distances, dtype = int)
		ratios = distances/d_tot
		number_points = np.array([int(x*k_points) for x in ratios])
		total_number_points = np.sum(number_points)

		while (total_number_points < k_points): 
			number_points[0]+=1
			total_number_points= np.sum(number_points)

		#Create irr. path
		kx, ky, kz = [],[],[]

		#Create k_path such that the each segment contains the high symmetry point at
		#the lower boundary and the last segment contains both high symmetry points
		for i in range(len(distances)):

			if i == len(distances)-1:

				kx = np.append(kx, np.linspace(high_sym[i][0], high_sym[i+1][0], number_points[i], endpoint= True)) 
				ky = np.append(ky, np.linspace(high_sym[i][1], high_sym[i+1][1], number_points[i], endpoint= True))
				kz = np.append(kz, np.linspace(high_sym[i][2], high_sym[i+1][2], number_points[i], endpoint= True))

			else:

				kx = np.append(kx, np.linspace(high_sym[i][0], high_sym[i+1][0], number_points[i], endpoint= False)) 
				ky = np.append(ky, np.linspace(high_sym[i][1], high_sym[i+1][1], number_points[i], endpoint= False))
				kz = np.append(kz, np.linspace(high_sym[i][2], high_sym[i+1][2], number_points[i], endpoint= False))

		k_path = []

		for i in range(k_points): 
			k_path += [np.array([kx[i], ky[i], kz[i]])]


		marker = [0]
		for i in range(len(distances)): 
			
			if i == len(distances)-1:

				new_marker = marker[-1]+number_points[i] -1 
				marker += [new_marker]

			else: 

				new_marker = marker[-1]+number_points[i] 
				marker += [new_marker]	

		marker = np.asarray(marker)

		return k_path, marker
