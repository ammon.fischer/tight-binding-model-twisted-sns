import numpy as np
import matplotlib.pyplot as plt

from momentum import Momentum
from tSnS import tSnS


def spin_texture(Nk=30, bandset=2): 

    """
    Compute Spin texture in BZ
    """

    #Pauli matrices and orbital identity
    sigma_x_tau_0 = np.array([[0,0,1,0],[0,0,0,1],[1,0,0,0],[0,1,0,0]])
    sigma_y_tau_0 = 1j*np.array([[0,0,-1,0],[0,0,0,-1],[1,0,0,0],[0,1,0,0]])
    sigma_z_tau_0 = np.array([[1,0,0,0],[0,1,0,0],[0,0,-1,0],[0,0,0,-1]])


    #Create Mesh and compute spin polarization
    mesh        = BZ.Bravais_mesh_simple(Nk)
    ekf         = np.zeros((Nk**2, model.N))
    ukf         = np.zeros((Nk**2, model.N, model.N), dtype=complex)
    spin_pol    = np.zeros((Nk**2, 3, model.N), dtype=complex)

    for i in range(len(mesh)):
        ekf, ukf        = np.linalg.eigh(model.BH_Hamiltonian(mesh[i], bandset=bandset))
        spin_pol[i,0,:] = (np.diagonal(ukf.T.conjugate()@sigma_x_tau_0@ukf)).real
        spin_pol[i,1,:] = (np.diagonal(ukf.T.conjugate()@sigma_y_tau_0@ukf)).real
        spin_pol[i,2,:] = (np.diagonal(ukf.T.conjugate()@sigma_z_tau_0@ukf)).real


    fig, axes = plt.subplots(3, model.N, sharex=True, sharey=True, figsize=(4,3))
    for i in range(3): 
        for j in range(model.N): 
            axes[i,j].scatter(mesh[:,0], mesh[:,1], c=spin_pol[:,i,j], cmap=plt.cm.RdBu, vmin=-1, vmax=1, s=1)
            axes[i,j].axis('scaled')
            axes[i,j].set_xticks([])
            axes[i,j].set_yticks([])

    axes[0,0].set_ylabel(r'$\langle S_x \rangle$')
    axes[1,0].set_ylabel(r'$\langle S_y \rangle$')
    axes[2,0].set_ylabel(r'$\langle S_z \rangle$')

    axes[0,0].set_xlabel(r'$n_b = 1$', labelpad=-70)
    axes[0,1].set_xlabel(r'$n_b = 2$', labelpad=-70)
    axes[0,2].set_xlabel(r'$n_b = 3$', labelpad=-70)
    axes[0,3].set_xlabel(r'$n_b = 4$', labelpad=-70)


    plt.subplots_adjust(hspace=0, wspace=0)
    plt.show()


if __name__ == '__main__':

    model = tSnS()
    BZ    = Momentum(model.G1, model.G2)
    spin_texture()
