import sys
import numpy as np 


class tSnS:
	

	"""
	Sets up the geometric properties of twisted SnS and generate Bloch Hamiltonian. 
	"""


	def __init__(self, **kwargs): 
		
		#Initialize supercell vectors
		self.A1 = np.array([40.718937498,4.292152880,0])
		self.A2 = np.array([-4.071893750,38.629375919,0])
		self.A3 = np.array([0,0,29.021099091])

		#Basis transformation matrix (two-dimensional)
		A_in_x = np.array([self.A1[:-1], self.A2[:-1]])
		x_in_A = np.linalg.inv(A_in_x)
		self.T = np.transpose(x_in_A)

		#Reciprocal lattice vectors
		self.volume =  np.linalg.norm(np.cross(self.A1, self.A2)) 
		self.G1 = 2*np.pi/self.volume*np.cross(self.A2, np.array([0.,0.,1.]))
		self.G2 = 2*np.pi/self.volume*np.cross(np.array([0.,0.,1.]), self.A1)
		self.G3 = -(self.G1+self.G2)
		self.volume_BZ = np.linalg.norm(np.cross(self.G1, self.G2)) 

		#Number of effective basis sites (sublattice + spin degrees of freedom)
		self.N = 4


	def phi_lm(self, momentum, l, m): 

		"""
		Phase factors of effective TB model
		"""

		lattice_vec = l*self.A1 + m*self.A2
		return np.exp(-1.j*np.dot(momentum, lattice_vec))


	def BH_Hamiltonian(self, k, bandset=2):

		"""
		Set up Bloch Hamiltonian at momentum point k.

		---------
		Arguments: k - 3d Array with coordinates of momentum point
				   bandset (optional) - Bandset I/II
		"""
		assert bandset in [1,2]

		pauli 	= np.array([[1,0,0,1],[0,1,1,0], [0,-1j,1j,0], [1,0,0,-1]], 
			dtype=complex).reshape(4,2,2)

		ham 	= np.zeros((2,2,2,2), dtype=complex)

		if bandset == 1: 

			popt = [-0.28159930344528794, 0.003968154118035716, 0.0030637767514870547, -0.00010069737918143397, 
					-0.004016436852695664, -0.0008890332864148689, -0.001246870994836976, 0.000848707931621465, 
					-0.0012602009377081112, -0.0007056692333075815, -0.0005572460653001037, -0.00045337702969447147, np.pi/30]

		else: 

			popt = [-0.32044985418524075,   0.002055488150036208,   0.0018580964148542333,  9.451241186629052e-05, 
					0.0055709631952728005, 0.0005463154794551496, 0.0006627259014835454, -0.000292263072270747, 
					-0.00029205370932945825, 0.0006839685785136236, -0.0021245180394823893, -0.0017931410640271215, np.pi/8]




		t_AA_0				= popt[0]
		t_AB_0, t_AB_1 		= popt[1], popt[2]
		t_AA_x, t_AA_y 		= popt[3], popt[4]
		t_AB_my, t_AB_py 	= popt[5], popt[6]
		t_AA_m, t_AA_p 		= popt[7], popt[8]
		t_AA_2y 			= popt[9]
		tR_AB_0, tR_AB_1 	= popt[10], popt[11]
		phi_AB 				= popt[12] 


		##################################################
		#Nearest-Neighbor/On-Site Coupling (kinetic terms)
		##################################################

		#Onsite
		ham[:,0,:,0] += t_AA_0*pauli[0]
		ham[:,1,:,1] += t_AA_0*pauli[0]

		#Nearest-Neighbor AA/BB coupling (2. nn)
		hop = t_AA_x*(self.phi_lm(k,1,0))*pauli[0]
		ham[:,0,:,0] += hop
		ham[:,0,:,0] += np.conjugate(hop).T
		ham[:,1,:,1] += hop
		ham[:,1,:,1] += np.conjugate(hop).T

		hop = t_AA_y*(self.phi_lm(k,0,1))*pauli[0]
		ham[:,0,:,0] += hop
		ham[:,0,:,0] += np.conjugate(hop).T
		ham[:,1,:,1] += hop
		ham[:,1,:,1] += np.conjugate(hop).T


		#################################################
		#Higher Neighbor Couplings (kinetic terms)
		#################################################

		###############
		#AA/BB coupling
		################
		#Corner states of first shell (3. nn)
		hop = t_AA_m*self.phi_lm(k,1,1)
		ham[:,1,:,1] += hop*pauli[0]
		ham[:,1,:,1] += np.conjugate(hop)*pauli[0]

		hop = t_AA_m*self.phi_lm(k,-1,1)
		ham[:,0,:,0] += hop*pauli[0]
		ham[:,0,:,0] += np.conjugate(hop)*pauli[0]

		hop = t_AA_p*self.phi_lm(k,1,1)
		ham[:,0,:,0] += hop*pauli[0]
		ham[:,0,:,0] += np.conjugate(hop)*pauli[0]

		hop = t_AA_p*self.phi_lm(k,-1,1)
		ham[:,1,:,1] += hop*pauli[0]
		ham[:,1,:,1] += np.conjugate(hop)*pauli[0]

		#Second shell (5. nn)
		hop = t_AA_2y*self.phi_lm(k,0,2)*pauli[0]
		ham[:,0,:,0] += hop
		ham[:,0,:,0] += np.conjugate(hop).T
		ham[:,1,:,1] += hop
		ham[:,1,:,1] += np.conjugate(hop).T

		###############
		#AB coupling
		################
		#Edge states of second shell (4. nn)
		hop = t_AB_my*(self.phi_lm(k,1,1)+self.phi_lm(k,1,-2))*pauli[0]
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		hop = t_AB_py*(self.phi_lm(k,0,1)+self.phi_lm(k,0,-2))*pauli[0]
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T			


		######################################
		#SOC Rashba-Coupling
		######################################
		#Nearest-neighbors (1. nn)
		hop = 1j*tR_AB_0*(-0.5*pauli[1]-0.5*pauli[2])
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		hop = 1j*tR_AB_0*(-0.5*pauli[1]+0.5*pauli[2])*self.phi_lm(k,0,-1)
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		hop = 1j*tR_AB_1*(0.5*pauli[1]-0.5*pauli[2])*self.phi_lm(k,1,0)
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		hop = 1j*tR_AB_1*(0.5*pauli[1]+0.5*pauli[2])*self.phi_lm(k,1,-1)
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T


		######################################
		#SOC Kane-Mele Coupling
		######################################
		#Nearest-neighbors (1. nn)
		km = np.array([[np.exp(1j*phi_AB),0],[0,np.exp(1j*phi_AB).conjugate()]])
		hop = t_AB_0*(1.+self.phi_lm(k,0,-1))*km
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		km = np.array([[np.exp(-1j*phi_AB),0],[0,np.exp(-1j*phi_AB).conjugate()]])
		hop = t_AB_1*(self.phi_lm(k,1,0)+self.phi_lm(k,1,-1))*km
		ham[:,0,:,1] += hop
		ham[:,1,:,0] += np.conjugate(hop).T

		ham = ham.reshape((4,4))

		assert np.allclose(ham, ham.conjugate().T, atol=1e-12)

		return ham
